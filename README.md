This is the **Scottish Metric Psalter** from 1650 as published (e.g.) by Jim Ross on
his website [Music for the Church of God](http://www.cgmusic.org/).

The presentation is transformed into USFM tagging. 

The text is divided into two files; the second contains the variants on several psalms.

#### Status:

1. Spellings to be reviewed.
2. Generate both OSIS files.
3. Add notes to link between versions.

